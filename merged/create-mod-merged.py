#!/usr/bin/python3

import json
from datetime import date
from os import path

base_json = "erp/erp.json"
files_to_merge = [
    "platform/platform-base.json",
    "platform/platform-conn.json",
    "retail/retail.json",
    "omni/omni-oms.json",
    "omni/omni-oms-externalmodules.json",
    "omni/omni-wms.json",
    "omni/omni-reporting.json",
    "localization/translations.json",
    "localization/localization-france.json",
    "localization/localization-italy.json",
    "localization/localization-portugal.json",
    "localization/localization-spain-cashvat.json",
    "retail/retail-pack.json"
]

mods_by_javapackage = {}
deps_by_javapackage = {}

# first pass over json: find & merge all mods (removing duplicates)
for to_merge in files_to_merge:
    print("1st pass over file %s" % to_merge)
    j2 = json.load(open(path.join('..', to_merge)))
    for mod in j2["mods"]:
        javapackage = mod["javapackage"]
        if javapackage not in mods_by_javapackage:
            mods_by_javapackage[javapackage] = mod

# second pass over json: deps entries, only add if not present in mod or dep
for to_merge in files_to_merge:
    print("2nd pass over file %s" % to_merge)
    j2 = json.load(open(path.join('..', to_merge)))
    # some file (i.e. retail-pack.json) don't have deps section
    if "deps" in j2:
        for dep in j2["deps"]:
            javapackage = dep["javapackage"]
            if javapackage not in mods_by_javapackage and javapackage not in deps_by_javapackage:
                deps_by_javapackage[javapackage] = dep

# Build output

# contains erp line (tip of pi)
j = json.load(open(path.join('..', base_json)))

j["desc"] = "Merged config of %s last update %s" % (files_to_merge, date.today())

j["mods"] = []
j["deps"] = []

def demoteModToDep(package):
    if mods_by_javapackage[package]:
        deps_by_javapackage[package] = mods_by_javapackage[package]
        del mods_by_javapackage[package]

# handle team.retail special case (they have clone of normal sampledata
# Remove normal sampledata module as having both will fail
del mods_by_javapackage["org.openbravo.retail.sampledata"]

# Handle special cases of conflicting modules

# caused by org.openbravo.service.integration.sapecc
# - It is adding a column EM_SAPECC_HAS_WRAPPER_SEGMENT to table OBEI_ENTITY_MAPPING
# - That causes dbconsistency problem of every module having:
#   - sourcedata shipped for OBEI_ENTITY_MAPPING
#   - does NOT depend on sapecc module
# - Workaround
#   - Skip testing of any such module (moving it to deps)
#     - That also disabled dbconsistency check for them avoiding the problem
demoteModToDep("org.openbravo.api")
demoteModToDep("org.openbravo.retail.api")
demoteModToDep("org.openbravo.retail.api.complementary")
demoteModToDep("org.openbravo.retail.api.france.ecotax")
demoteModToDep("org.openbravo.retail.api.giftcards")
demoteModToDep("org.openbravo.retail.api.multiupc")
demoteModToDep("org.openbravo.distributionorder.api")
demoteModToDep("org.openbravo.service.external.integration.tests")
demoteModToDep("org.openbravo.service.integration.magento.mappings")
demoteModToDep("org.openbravo.warehouse.advancedwarehouseoperations.edl")

# remove org.openbravo.masterdata.uniqueconstraintbylegalentity
## its junit tests fail when running in mod-merged*
demoteModToDep("org.openbravo.masterdata.uniqueconstraintbylegalentity")

# remove org.openbravo.certification.france.testing (not needed or wanted in -merged)
del mods_by_javapackage["org.openbravo.certification.france.testing"]

# sorted output for mods
for javapackage in sorted(mods_by_javapackage.keys()):
    mod = mods_by_javapackage[javapackage]
    j["mods"].append(mod)

# sorted output for deps
for javapackage in sorted(deps_by_javapackage.keys()):
    dep = deps_by_javapackage[javapackage]
    j["deps"].append(dep)

print("Count mods: %d" % len(j["mods"]))
print("Count deps: %d" % len(j["deps"]))

json.dump(j, open("merged.json", "w"), indent="\t")
