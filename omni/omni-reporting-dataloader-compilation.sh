#!/bin/bash

set -e

CURRENT_WS=`pwd`
WORKSPACE=$CURRENT_WS
MODULE='org.openbravo.reporting.tools'
cd $WORKSPACE/modules/$MODULE

cp reporting.properties.template reporting.properties

OUTPUT_DIR=$WORKSPACE/SANDBOX/dataloader_output

[ -d $OUTPUT_DIR ] && rm -r $OUTPUT_DIR
mkdir -p $OUTPUT_DIR

set +e
ant generate.dataloader.application -DtargetDir=$OUTPUT_DIR
RESULT=$?
set -e

if [ "$RESULT" != "0" ] ; then
   echo -e "\n\n\n * FAILURE:  generate.dataloader.application -DtargetDir=$OUTPUT_DIR" | tee -a $WORKSPACE/errors.log
   echo -e "RESULTS in $OUTPUT_DIR" | tee -a $WORKSPACE/errors.log
fi

[ -d /tmp/reportingSources ] && rm -r /tmp/reportingSources
[ -d /tmp/reportingBuild ] && rm -r /tmp/reportingBuild

exit 0

