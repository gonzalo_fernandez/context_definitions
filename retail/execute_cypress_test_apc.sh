#!/bin/bash

set -ex

CURRENT_WS=`pwd`
WORKSPACE=$CURRENT_WS
MOD=org.openbravo.core2
CONTEXT=$(cat config/Openbravo.properties | grep -i context.name= |cut -d "=" -f2)

cd $WORKSPACE
npm install

if [ ! -d modules/$MOD ] ; then
   echo "Error Missing module org.openbravo.core2"
   exit 1
else
   TEST_WS="modules/$MOD/web-jspack/org.openbravo.core2/src-test"
   cd $TEST_WS
   echo "In path : `pwd`"
   cp cypress.env.json.template cypress.env.json

   # configure urls for cypress test
   baseurl="http://localhost/$CONTEXT/"
   jq --arg a "$baseurl" '.baseUrl = $a' cypress.env.json > tmp.json
   mv tmp.json cypress.env.json
   terminalurl="/web/pos/?terminal=VBS-2"
   jq --arg a "$terminalurl" '.terminalUrl = $a' cypress.env.json > tmp.json
   mv tmp.json cypress.env.json

   # execute test
   export DISPLAY=:3
   SPEC_PATH="$WORKSPACE/modules/**/web-jspack/**/src-test/cypress/integration/AllSpecs/all.spec.js"
   $WORKSPACE/node_modules/cypress/bin/cypress run \
           --spec $SPEC_PATH \
           --browser chrome \
           --reporter junit \
           --reporter-options "mochaFile=$CURRENT_WS/cypress-test-output-[hash].xml,toConsole=true,rootSuiteTitle=CypressSuite,testsuitesTitle=E2ESuites,jenkinsMode=true"
fi

exit 0
